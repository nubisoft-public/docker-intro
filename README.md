# Docker Intro

Sample `Dockerfile`s used in SkillUp presentation about Docker.

Link to slides: https://docs.google.com/presentation/d/1vL5gSkXqkOivxtq_GikaLHvMjr-oKLwV8IHIWTBZl0E/edit?usp=sharing

# CMD and ENTRYPOINT

Build image with:

```shell
$ docker build -t skillup1 -f Dockerfile_cmd ./
```

Run:

```shell
$ docker run -it --rm skillup1
```

Combination of `ENTRYPOINT` and `CMD`:

```shell
$ docker build -t skillup11 -f Dockerfile_cmd_entrypoint ./
$ docker run -it --rm skillup11 -I https://nubisoft.io

```

# Expose ports
Build an image exposing HTTP server on port `8000`:

```shell
$ docker build -t skillup2 -f Dockerfile_expose ./
```

Start container that exposes `8000` on random host port:
```shell
$ docker run -it --rm -P skillup2
```

Start container that exposes `8000` on host:
```shell
$ docker run -it --rm -p "8000:8000" skillup2
```

# Running containers with dedicated user

Build an image:
```shell
$ docker build -t skillup3 -f Dockerfile_user .
```

Run the container:
```shell
$ docker run -it --rm skillup3 .
```

Run the container with volume mount:
```shell
$ docker run -it --rm -v "/tmp/skillup:/tmp" skillup3
```
